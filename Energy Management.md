# Manage Energy not Time

## Q1. What are the activities you do that make you relax - Calm quadrant?

- There are some activities that make me relax are -
    1. Playing with my kid.
    2. Listening to music, especially Bhajans.
    3. Watching action or suspense movies.

## Q2. When do you find getting into the Stress quadrant?

- When I am unable to do any work before the deadline, unable to solve any problem, and not getting any results from utilizing a different approach, then I find myself in a stressful situation.

## Q3. How do you understand if you are in the Excitement quadrant?

- Whenever I am having a plan to execute and while executing it all goes well and expected results are occurring, I feel very excited.

# Sleep is your superpower

## Q4. Paraphrase the Sleep is your Superpower video in detail.

- Sleep is an essential activity of one's life, One should take a sufficient amount of sleep in order to remain healthy. Studies have shown people with less sleep are prone to have heart attacks, our immune systems are also gets affected, and aging is also common to sleep deprivation. Lack of sleep can result in major health issues.

## Q5. What are some ideas that you can implement to sleep better?

- Relaxing your body, fixing a time to sleep, room temperature, exercising, and following a routine for having supper can help in better sleep.

# Brain Changing Benefits of Exercise

## Q6. Paraphrase the video - Brain Changing Benefits of Exercise.

- There are many Brain Changing Benefits of Exercise, some of them are-
    - Maintaining focus longer than earlier.
    - Aging gets slow, and incurable diseases take longer to hit.
    - Helps reduce obesity and makes you feel good.
    - Helps you in relaxing.
    - Helps in tackling stress.

## Q7. What are some steps you can take to exercise more?

- It is not required to take a membership in the GYM to do exercise or need to be a pro athlete to do exercise. It only takes some effort and willingness to do so. You can simply add some physical activities like jogging an extra mile, spot jumping, hand movements, and some aerobic exercises to pump your heart up a little.
