# Tiny Habits - BJ Fogg

### Q1. Your takeaways from the video.

- In order to change long-term behavior we need to work on our tiny habits, one can follow the following points-
	
	- Keep up the motivation at the start.
	- Start with tiny work, as easy tasks require very less motivating.
	- Keep a reward ready after the tiny work.
	- Once the task is done, celebrate and reward yourself.
	- Choose an easy trigger to keep up the good work.

# Tiny Habits by BJ Fogg - Core Message

### Q2. Your takeaways from the video in as much detail as possible.

- To do behavior change one needs to start with tiny habits, it's a time taking process but you will not notice the time when you will have a behavior change. In order to achieve the behavior change one should follow the B=MAP formula, where B is behavior, M is motivation, A is the ability and P is prompt or trigger. Always celebrate your success, keep tasks easy and simple, use easy triggers, and plan your task according to your behavior change.


### Q3. How can you use B = MAP to make making new habits easier?

- B=MAP formula, where B is behavior, M is motivation, A is the ability and P is prompt or trigger, can be followed to achieve behavior change, one should plan tiny habits according to behavior change and keep triggers easy and simple, choose tiny habits according to behavior change as easy, small and doable and enjoy the completion of habit in order to keep motivation pumped up.


### Q4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

- In order to keep one motivated and pumped up, one should celebrate the successful completion of tasks. As this small celebration keeps the person motivated and encourages him to add up some complexity to the task.

# 1% Better Every Day Video

### Q5. Your takeaways from the video.

- Long-term changes can not be achieved in one day it is a time taking and step-by-step process, in order to achieve behavior changes one needs to plan and simply work on little improvements every day.
Getting tiny tasks successful one need to work on the start and make it easy and simple. A little improvement every day lead to achieving bigger goals in the long term process.

# Book Summary of Atomic Habits

### Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

- The book suggested many ways of adapting any habit and making that habit your style of living, for doing this, the first step is to identify the habit and why you need that to adapt?, second step is to architect the path and generate a process to achieve your goal of adopting that habit, the third and last step is to reward your self with the outcomes.

### Q7. Write about the book's perspective on how to make a good habit easier?

- In order to make good habits easier to adapt and doable one should make the process of doing that habit easier. Doing that habit for less amount of time and rewarding yourself leads you to adopt that habit easily and quickly. The most important part of adopting any habit is to start, this is very crucial as many fails at the start, so architect your start in such a way that it feels easy and doable to start using that habit.

### Q8. Write about the book's perspective on making a bad habit more difficult?

- In order to make the bad habit more difficult is to keep them apart and make it difficult to reach them, never keep your bad habits near your good habits or connected. Reaching your bad habit should be difficult and hard to start.

# Reflection:

### Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I would like to work on more projects, I want to learn and explore frontend profiles, so I would plan my weekends according to that. I would connect more with my friends who are already in front-end profiles and look for the projects that they already did.

### Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- I would want to stay away from sugar as it is making me overweight, in order to achieve my goal I am planning to adopt alternatives to sugar like Honey. I am really worried about my weight that is the reason I have stopped consuming coffee.
