# database (DB) 

## What is a database?

A database is information that is set up for easy access, management and updating. Computer databases typically store aggregations of data records or files that contain information, such as sales transactions, customer data, financials and product information.

Databases are used for storing, maintaining and accessing any sort of data. They collect information on people, places or things. That information is gathered in one place so that it can be observed and analyzed. Databases can be thought of as an organized collection of information.


## What are databases used for?

Businesses use data stored in databases to make informed business decisions. Some of the ways organizations use databases include the following:

   - **Improve business processes.** Companies collect data about business processes, such sales, order processing and customer service. They analyze that data to improve these processes, expand their business and grow revenue.
   - **Keep track of customers.** Databases often store information about people, such as customers or users. For example, social media platforms use databases to store user information, such as names, email addresses and user behavior. The data is used to recommend content to users and improve the user experience.
   - **Secure personal health information.** Healthcare providers use databases to securely store personal health data to inform and improve patient care.
   - **Store personal data.** Databases can also be used to store personal information. For example, personal cloud storage is available for individual users to store media, such as photos, in a managed cloud.

## Types of databases

There are many types of databases. They may be classified according to content type: bibliographic, full text, numeric and images. In computing, databases are often classified based on the organizational approach they use.

Some of the main organizational databases include the following:

**Relational.** This tabular approach defines data so it can be reorganized and accessed in many ways. Relational databases are comprised of tables. Data is placed into predefined categories in those tables. Each table has columns with at least one data category, and rows that have a certain data instance for the categories which are defined in the columns. 

**Distributed.** This database stores records or files in several physical locations. Data processing is also spread out and replicated across different parts of the network.

**Cloud.** These databases are built in a public, private or hybrid cloud for a virtualized environment. Users are charged based on how much storage and bandwidth they use. They also get scalability on demand and high availability.

**NoSQL.** NoSQL databases are good when dealing with large collections of distributed data. They can address big data performance issues better than relational databases. They also do well analyzing large unstructured data sets and data on virtual servers in the cloud. These databases can also be called non-relational databases.

**Object-oriented.** These databases hold data created using object-oriented programming languages. They focus on organizing objects rather than actions and data rather than logic. For instance, an image data record would be a data object, rather than an alphanumeric value.

**Graph.** These databases are a type of NoSQL database. They store, map and query relationships using concepts from graph theory. Graph databases are made up of nodes and edges. Nodes are entities and connect the nodes.

<hr>

## SQL vs NoSQL

When choosing a modern database, one of the biggest decisions is picking a relational (SQL) or non-relational (NoSQL) data structure. While both are viable options, there are key differences between the two that users must keep in mind when making a decision.

Here, we break down the most important distinctions and discuss the best SQL and NoSQL database systems available.

The five critical differences between SQL vs NoSQL are:

    1. SQL databases are relational, NoSQL databases are non-relational.
    2. SQL databases use structured query language and have a predefined schema. NoSQL databases have dynamic schemas for unstructured data.
    3. SQL databases are vertically scalable, while NoSQL databases are horizontally scalable.
    4. SQL databases are table-based, while NoSQL databases are document, key-value, graph, or wide-column stores.
    5. SQL databases are better for multi-row transactions, while NoSQL is better for unstructured data like documents or JSON.
<hr>

## Popular relational databases and RDBMSs

The following list describes popular SQL and RDBMS databases:

   - **Oracle®:** An object-relational database management system (DBMS) written in the C++ language.

   - **IBM DB2®:** A family of database server products from IBM®.

   - **SAP ASE®:** A business relational database server product for primarily Unix® operating systems.

   - **Microsoft SQL Server®:** An RDBMS for enterprise-level databases that supports both SQL and NoSQL architectures.

   - **Maria DB®:** An enhanced, drop-in version of MySQL®.

   - **PostgreSQL®:** An enterprise-level, object-relational DBMS that uses procedural languages, such as Perl and Python, in addition to SQL-level code.
   
<hr>  

## Popular NoSQL databases

The following list describes popular NoSQL databases:

   - **MongoDB®:** The most popular open-source NoSQL system. MongoDB is a document-oriented database that stores JSON-like documents in dynamic schemas.

   - **Apache CouchDB®:** An open-source, web-oriented database developed by Apache®. CouchDB uses the JSON data exchange format to store its documents; JavaScript for indexing, combining, and transforming documents; and HTTP for its API.

   - **Apache HBase®:** An open-source Apache project developed as a part of Hadoop®. HBase is a column store database written in Java with capabilities similar to those that Google BigTable® provides.

   - **Oracle NoSQL Database®:** A proprietary database that supports JSON table and key-value datatypes running on-premise or as a cloud service.

   - **Apache Cassandra DB®:** A distributed database that excels at handling extremely large amounts of structured data. Cassandra DB is also highly scalable. Facebook® created Cassandra DB.

   - **Riak®:** An open-source, key-value store database written in Erlang. Riak has built-in fault-tolerance replication and automatic data distribution that enable it to offer excellent performance.

   - **Objectivity InfiniteGraph®:** A highly specialized graph database that focuses on graph data structures. InfiniteGraph, implemented in Java, is useful for finding hidden relationships in big data.
   <hr>

## Some NoSQL Databases and their uses

### MongoDB 

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/MongoDB_Logo.svg/512px-MongoDB_Logo.svg.png?20190626143224)

MongoDB is an open-source document database built on a horizontal scale-out architecture that uses a flexible schema for storing data. Founded in 2007, MongoDB has a worldwide following in the developer community.

Instead of storing data in tables of rows or columns like SQL databases, each record in a MongoDB database is a document described in BSON, a binary representation of the data. Applications can then retrieve this information in a JSON format.

Here’s a simple JSON document describing a historical figure.
```
{
  "_id": 1,
  "name": {
    "first": "Ada",
    "last": "Lovelace"
  },
  "title": "The First Programmer",
  "interests": ["mathematics", "programming"]
}
```
### Real-World MongoDB Use Cases

- **MongoDB Use Case 1: Product Data Management**

MongoDB is perfect for Product Data Management. It enables product data and related information to be managed and processed in a single, central system. This allows for Detailed Cost Analysis, Increased Productivity, and Improved Collaboration. 

- **MongoDB Use Case 2: Operational Intelligence**

Another real-world MongoDB use case is Operational Intelligence, as it aids in Real-time Decision-making. It allows companies to seamlessly gather various data feeds representing their ongoing business operations and information of related external factors. They can then analyze these feeds as the data arrives for developing profitable and functional business strategies.

- **MongoDB Use Case 3: Product Catalog**

Product catalogs have been in existence for years in the ever-evolving digital space. However, with the rapid evolution in technology, product catalogs sometimes feel like a new digital experience. This is because the richness and volume of data feed product catalogs’ interactions today are remarkable.

MongoDB is useful in such applications, as it provides an excellent tool for storing different types of objects. In addition, its Dynamic Schema Capability ensures that product documents only contain attributes relevant to that product.

- **MongoDB Use Case 4: Scaling And Application Mobility**

For most Mobile Application Development, it is expected that the companies involved will have to deal with different data structures from several sources and potentially highly dynamic growth. Interestingly, MongoDB provides High Flexibility and Scalability that serves as an excellent database solution for such challenges. In addition, it allows developers to focus on developing a better customer experience, instead of spending time adjusting the database. 

- **MongoDB Use Case 5: Customer Analytics**

Creating an exceptional customer experience is essential to staying relevant in the market and competing healthily with the competition. This has increased people’s expectations of what a good customer experience should look like over the past few years. Companies are ready to go out of their way to meet and exceed their customers’ expectations. 

Data aggregation allows many companies to create exceptional customer experiences. This is because they can collect massive amounts of data of their potential and existing customers, and add it with publicly available data.

With this, they will have clearer insights into how their customers interact with products. It further allows them to make effective strategies on how to satisfy their customers and retain them. MongoDB is perfect for Data Aggregation and Building Analytical Tools for creating exceptional customer experiences.

- **MongoDB Use Case 6: Mainframe Offloading**

Despite the high operational cost and new databases dominating the market, Mainframe remains essential in infrastructure. This is because moving data off the Mainframe is a challenging task. However, MongoDB is efficient in offloading data from mainframe systems at reduced operational cost.

- **MongoDB Use Case 7: Real-Time Data Integration**

Companies usually have huge amounts of data distributed across their organization. These data are valuable when aggregated in a single view. MongoDB provides top-notch flexibility and query capabilities that make aggregating and organizing data more efficient.

### Real-life Examples Of MongoDB Use Cases

You have looked at some of the MongoDB use cases, so now let’s discuss a few real-life applications of this database system. Here are a few real-life applications Of MongoDB By Some Prominent Companies.
- **1) India’s Aadhaar**

Aadhaar is a fantastic example of a real-world MongoDB use case. Aadhaar is India’s Unique Identification Project and the world’s most extensive Biometrics Database System. The program launched in 2009 has collected demographic and biometric information from over 1.2 billion people. Aadhaar relied on MongoDB, among other database systems like HBase, MySQL, and Hadoop, to store this massive amount of data. However, MongoDB was one of the database systems first purchased to power the search strategy.

- **2) Forbes**

Another real-world MongoDB use case is Forbes. When a story becomes viral, people resort to every available website to get details. Therefore, it is essential for publishers to be alert and give shareable information as soon as possible to keep their readers informed.

This prompted Forbes to find practical solutions to boost its Content Engagement Rate. Forbes constructed its CMS and mobile application in two months using MongoDB. They had to redesign their website and switch to MongoDB to upload content from anywhere in the world efficiently.

Their editors also took advantage of MongoDB’s flexible structure to deliver dynamic quality material promptly to their viewers. This brave action eventually paid off. With this, they could eliminate poor existing practices while lowering overhead expenses.

- **3) MetLife**

MetLife is a market leader in employee insurance, benefit plans, and pensions. They serve over 90 million clients across the Middle East, Europe, Asia, Latin America, Japan, and the United States. MetLife’s sophisticated customer service solution, “The Wall,” is built on MongoDB.

The Wall is a tool that offers a consolidated view of MetLife Customers’ payments, policy statements, and other information. It functions like Facebook’s Wall, which gathers data from 70 historical systems and integrates it into a database table. The Wall has a capacity of 24 TB and is distributed across six different servers in two Data Management Centers. Currently, MetLife is looking to further improve its service with several Big Data initiatives, including MongoDB-based apps.

- **4) Otto**

Many retail businesses need Real-time Analytics. This requires that their apps remain relevant and always available online, with no interruption. This is because slow response time leads to a lot of revenue loss in the retail industry due to the cutthroat competition.

When consumers have several alternatives, they will not hesitate to switch to a more reliable service provider. For example, Otto is a major e-commerce company with a massive user base looking to meet and exceed its customers’ expectations. However, prompt response time was a significant challenge because their website has over 500 companies.

Otto is a classic MongoDB use case where they used MongoDB to lower their response time to between 1 to 2 seconds to solve this problem. They had to rebuild their entire catalog application. They used MongoDB because of its customizable schema capabilities, reliability, and growth characteristics.

- **5) Shutterfly**

Shutterfly is a major Digital Picture Exchange and Private Publishing Firm with over 6 billion photographs and a processing volume of up to 10,000 calculations every second. It is one of the businesses that switched from Oracle to MongoDB.

During their transition to MongoDB, it became clear that running non-Relational Databases would better meet the company’s data demands, potentially enhancing programmer efficiency and customizability.

Before choosing MongoDB, Shutterfly investigated several other database systems, including BerkeleyDB and Cassandra. However, the firm settled for MongoDB while implementing it to data relevant to uploaded photographs. Still, the conventional RDBMS remains in place for aspects of the process that require a fuller transactional architecture, such as payment and account administration.

Shutterfly is pleased with its choice to adopt MongoDB, as noted by its data architect, Kenny Gorman, who said the firm believes in picking the right technology for the task. His statement showed that MongoDB was an excellent match, although not without tradeoffs. For example, many issues cropped up before they streamlined their services to match their users’ needs.

- **6) FACEIT**

Major gaming firms like FACEIT and SEGA have effectively used MongoDB to improve their consumers’ interactive experiences. Underneath the shell, FACEIT relies on MongoDB as its primary Database System. In addition, MongoDB manages the orchestration of services between gamers, groups, and contests.

FACEIT even uses MongoDB to maintain its User Profile and Tournament Data. Users’ live streaming data is saved in MongoDB, and other metrics are used to measure players’ activity and interaction. Due to MongoDB’s Flexible Structure and Extensive Query Model, FACEIT could keep user profiles more effectively.

- **7) Weather Channel**

While running its website, weather.com, the Weather Channel had problems servicing a massive clientele because it used a standard RDBMS system. However, MongoDB enabled them to develop their Smartphone application that serves over 40 million active users and provides Real-time Meteorological Data to its clients.

They also employed MongoDB’s Scalable Solution and MapReduce Functionalities to perform Real-time Analytics and forecasts based on weather data obtained. The prototype version that had previously taken weeks to develop was delivered in just a few hours. This gave The Weather Channel an edge over the competition.
<hr>

### Apache CouchDB

![](https://svn.apache.org/repos/asf/couchdb/supplement/logo/couchdb-site-brown.png)

Apache CouchDB is a document-oriented, open-source, single node NoSQL database that allows you to store your data with JSON documents and easily access it through a web browser. CouchDB uses multiple protocols and formats to store, process and transfer data. For demanding projects, CouchDB can scale up into a cluster of nodes with multiple servers. It has been designed with a crash-resistant structure and reliability that supports “offline first” apps and a system that redundantly saves data to keep it secure and available in case of emergency.

### Some use cases:

- **ACID Semantics**

CouchDB provides ACID semantics. It does this by implementing a form of Multi-Version Concurrency Control, meaning that CouchDB can handle a high volume of concurrent readers and writers without conflict.

- **Built for Offline**

CouchDB can replicate to devices (like smartphones) that can go offline and handle data sync for you when the device is back online.

- **Distributed Architecture with Replication**

CouchDB was designed with bi-directional replication (or synchronization) and off-line operation in mind. That means multiple replicas can have their own copies of the same data, modify it, and then sync those changes at a later time.

- **Document Storage**

CouchDB stores data as "documents", as one or more field/value pairs expressed as JSON. Field values can be simple things like strings, numbers, or dates; but ordered lists and associative arrays can also be used. Every document in a CouchDB database has a unique id and there is no required document schema.

- **Eventual Consistency**

CouchDB guarantees eventual consistency to be able to provide both availability and partition tolerance.

- **Map/Reduce Views and Indexes**

The stored data is structured using views. In CouchDB, each view is constructed by a JavaScript function that acts as the Map half of a map/reduce operation. The function takes a document and transforms it into a single value that it returns. CouchDB can index views and keep those indexes updated as documents are added, removed, or updated.

- **HTTP API**

All items have a unique URI that gets exposed via HTTP. It uses the HTTP methods POST, GET, PUT and DELETE for the four basic CRUD (Create, Read, Update, Delete) operations on all resources.

CouchDB also offers a built-in administration interface accessible via Web called Futon. 
<hr>

### Apache HBase

![](https://miro.medium.com/max/720/0*oy5njOjm2ad3g-Ul)

HBase is a column-oriented, open-source, distributed, non-relational database that works on the Hadoop Database File System. Apache HBase can be used when you randomly need real-time read/write access to Big Data. It provides linear scalability, consistent read/write, automatic configurable table sharding, easy-to-use Java API for client access, real-time queries and so much more. HBase is based on Google’s Bigtable, a distributed storage system for structured data to offer Bigtable-like capabilities for applications.

#### Some of the specific use cases:

  - **E-commercial ordering system.**

Daily orders and user actions can be stored in HBase for low-latency random access on massive number of orders. Historical order data can also be stored in HBase to run offline analysis jobs via computing frameworks like MapReduce, Spark, Flink, etc.

  - **Search engine.** 

Raw content of web-links along with analytical feature data can be stored in HBase to compute search result ranking, etc.

  - **Log aggregation and metrics.** 

OpenTSDB (Open Time Series Database), for example, is one open-sourced database system dedicated to log aggregation and metrics collection.
<hr>

### Apache Cassandra 

![](http://bi-insider.com/wp-content/uploads/2022/02/apache-cassandra-logo-705x142.png)

Apache Cassandra is an open-source distributed NoSQL database that offers high scalability and availability without affecting the performance of the application. Apache Cassandra manages unstructured data with thousands of writes every second. Proven fault tolerance and linear scalability on cloud infrastructure or commodity hardware make Cassandra a perfect choice for mission-critical data. To assure scalability and reliability, Cassandra is tested on 1000 node clusters, 100 real-world schemas and use cases by Apple, Netflix, Amazon and others. Besides, it can even handle failed replacements of nodes without shutting down the system. Cassandra also replicates data across multiple nodes automatically, making it an ideal choice for developers.

### Here are some use cases we see often:

    
- **E-commerce and inventory management**

E-commerce companies can’t afford to have their site go down, and that’s especially true during a peak period. Every minute they’re offline quickly eats away at their bottom line. Since rapid growth is always the goal, they also need the ability to cost-effectively scale their online inventory on the fly. For the same reason, these organizations need a database that can handle an enormous amount of data with ease. And to meet or exceed customer expectations, they need the flexibility to continuously modify their product mix.

Here’s why Cassandra is a good fit for e-commerce and inventory management:

    - Resilient with zero downtime: Distributed with multi-region replication, Cassandra ensures zero downtime. Even the loss of an entire region won’t bring it down.
    - Highly responsive: Cassandra’s peer-to-peer architecture also allows data to reside in regions around the world and closer to any particular customer—allowing the system to be highly responsive and fast.
    - Predictable scalability: Cassandra’s horizontal scalability is straightforward, predictable, and cost-effective.
    - Provides faster catalog refreshes.
    - Analyzes its catalog and inventory in real time. 

- **Personalization, recommendations, and customer experience**

Personalization and recommendation engines are everywhere now. Almost like personal assistants built-into apps and websites, they help us decide what events to buy tickets to, surfacing articles we might find interesting, and much more. Eventbrite now uses Cassandra instead of MySQL to power their mobile experience, letting users know what events are happening around them that they will be interested in attending. Eventbrite chose Cassandra for its read/write capacity and ease of deployment. Outbrain, a company you use frequently, but may be unfamiliar with, uses Cassandra to power their content discovery platform, helping companies add revenue streams by serving up applicable third-party articles you may find interesting. 

Near real-time, relevant, personalized experiences are now expected. Here’s why Cassandra is the right choice to power tailored experiences:

    - Fast response times.
    - Extremely low latency, even as your customer base expands.
    - Handles all types of data, structured and unstructured, from a variety of sources.
    - Built to scale while staying cost-effective.
    - Ability to store, manage, query, and modify extremely large datasets, while delivering personalized experiences to millions of customers.
    - High read/write capacity.
    - Ease of deployment.
    - Flexible, enabling continuous customer experience innovation.


- **Internet of things and edge computing**


Whether tracking weather, traffic, energy consumption, inventory levels, health indicators, video game stats, farming conditions or countless other metrics, internet of things (IoT) sensors, wearables, vehicles, mobile devices, appliances, drones, and other devices at the edge produce an avalanche of never-ending data. This data needs to be securely collected—sometimes from millions of devices—aggregated, processed, and analyzed on an ongoing basis.

Consider how the National Renewable Energy Laboratory uses Cassandra to store and analyze sensor data at the world's most environmentally friendly building. They find ways to save water and energy by running the world's smartest thermostat on top of Cassandra. The system continuously learns about energy usage patterns, and automatically adjusts settings, even when no one is there to program it.

Here are some of the reasons Cassandra is a good fit for IoT and edge computing needs:

    - Cassandra can ingest concurrent data from any node in the cluster, since all have read/write capacity.
    - Ability to handle a large volume of high-velocity, time-series data.
    - High availability.
    - Supports continuous, real-time analysis.

- **Fraud detection and authentication**

Security threats continue to rise, and many companies are always on the defensive, playing catch-up with their smart fraud detection capabilities. That’s because fraudsters are constantly on the attack, looking for new and creative ways to steal customer data and compromise other sensitive information.

To have any chance of preventing illegitimate users from gaining access, companies need data and a lot of it. Continuous, real-time analysis of large and diverse datasets is required to find patterns and anomalies that can be indicators of fraud. A high priority for all businesses, fraud detection’s importance is elevated in areas like financial services, banking, payments, and insurance. As an example, take a look at how ACI Worldwide has used Cassandra to drastically improve its fraud detection rate and false positive rate.

Identity authentication is the other side of the fraud detection coin. Instead of focusing on keeping fraudsters out, the goal of authentication is to confirm that only legitimate customers gain access. The trick is, you want to make the log-in process as painless and fast as possible, while still making absolutely sure they are who they say they are. As with fraud detection, to pull this off, you need to conduct real-time analysis of a wide variety and high volume of data. And since authentication is likely a central part of all your systems, outages must be avoided at all costs. If a customer experiences friction trying to access your site, whether due to a false positive or because the auth system is down, it likely won’t take too long for them to leave in frustration.

Here are some reasons Cassandra is a great database choice for fighting fraud and ensuring identity authentication:

    - Flexible schema: Handles numerous data types, and they can be quickly added to the mix.
    - Enables complex, real-time analysis, including the ability to incorporate and support machine learning and AI.
    - Handles large-scale, growing datasets.
<hr>

### Riak 

![Riak](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHCxYlscl0yu0vQNB2PLxQozlT8bdWxPWMHw&usqp=CAU)

Founded in 2008, Riak was one of the first distributed systems that simplifies the most critical data management challenges for enterprises. It comes in two variants: Riak KV and Riak TS. Even though both variants share the same Riak Core, both have different use cases. 

Riak KV is a highly available, scalable and easy to operate distributed NoSQL database. One of the major things about it is the fact that it automatically distributes data across the cluster to ensure fast performance and fault-tolerance. Also, Riak KV deals with Key-Value use cases. While on the other hand, Riak TS optimizes time series and IoT data. It also provides faster reads and writes that helps in making it easier to store, query, and analyze time and location data. So, it basically forms the foundation for reducing the complexity of integrating and deploying active workloads for Big Data, IoT and hybrid cloud applications with today’s most flexible and available NoSQL database.


#### Use cases Of Riak

- **For Session Storage**

Riak was originally created to serve as a scalable session store. However, over the years the database evolved and started serving some of the complex session storages too. Why? Riak is loaded with some of the advanced features such as Bitcask, MapReduce etc. For example, as user and session IDs are usually stored in cookies, Riak is able to serve these requests with predictably low latency. And it is one of the important use cases for Riak.

- **For Ads**

Riak has proved time and again that it is one of the best choices for a lot of use cases and serving advertising content is one of the major ones. Being such a popular database, Riak is able to serve content to different web and mobile users simultaneously with low latency.

- **Log Data**

Log data refers to the information of events that occur in an OS or other software, or messages between different users of communication software. And one of the common yet important use cases of Riak is storing large amounts of log data, and that can be done with the help of MapReduce.

- **For Sensor Data**

Sensor data is basically the output of a device that detects and responds to some type of input from the physical environment. Many devices collect and send data at a given interval and sometimes it might be cumbersome to handle that data. And this where Riak comes into the scenario, it is considered to be one of the good options to store sensor data.

- **User Account Data** 

User account data is critical for every company to handle. Therefore, companies need a simple, yet effective way to store that data. Here in this case, Riak can be a good storing option too; each account data in Riak could be stored  as a JSON object in a bucket. And further, object keys for the data objects can be used. And in order to retrieve data, a user ID can be used.
<hr>

## Resources

- [What is a database?](https://www.techtarget.com/searchdatamanagement/definition/database)
- [SQL vs NoSQL](https://www.integrate.io/blog/the-sql-vs-nosql-difference/)
- [Examples of RDBMS and NoSQL databases](https://docs.rackspace.com/support/how-to/examples-of-rdbms-and-nosql-databases/)
- [Top NoSQL Databases in 2022](https://www.decipherzone.com/blog-detail/nosql-databases)
- [What is Riak?](https://analyticsindiamag.com/is-riak-a-good-nosql-database-option/)
- [Apache HBase — a Brief Introduction](https://medium.com/@wangwei09310931/apache-hbase-a-brief-introduction-7e2e3a1bbc91)
- [Apache Cassandra – NoSQL Database](http://bi-insider.com/posts/apache-cassandra-nosql-database/)
- [Exploring Common Apache Cassandra Use Cases](https://www.datastax.com/blog/exploring-common-apache-cassandra-use-cases)
- [Apache CouchDB](https://en.wikipedia.org/wiki/Apache_CouchDB)
- [Best 7 Real-World MongoDB Use Cases](https://hevodata.com/learn/mongodb-use-case/)
